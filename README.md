# _Hello World_

#### _A website showing how to write "Hello, World" in different languages, March 14, 2016_

#### By _**Nick Lyman**_

## Description

_A simple webpage displaying "Hello, World" in different languages._

## Setup/Installation Requirements

* _Link to repository: https://github.com/nicklyman/0314_Intro_hello-world.git_
* _Clone this repository to your Github account_
* _Create a project directory up on your computer_
* _Open the .hmtl file in your web browser_
* _Open the .html file in a text editor_

## Known Bugs

_No known bugs_

## Support and contact details

_If a bug is found, please let me know via Github. Feel free to contact me with questions or suggestions and contribute to the code._

## Technologies Used

* _Git_
* _Github_
* _Atom text editor_
* _HTML_

### License

*This software is licensed under the MIT license*

Copyright (c) 2015 **_Nick Lyman_**
